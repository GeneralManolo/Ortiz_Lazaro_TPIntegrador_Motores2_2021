﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour
{
    public GameObject Player1SpawnPoint;
    public GameObject Player2SpawnPoint;
    public GameObject Player3SpawnPoint;
    public GameObject Player4SpawnPoint;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.transform.position = Player1SpawnPoint.transform.position;
        }

        if (other.gameObject.CompareTag("Player2"))
        {
            other.transform.position = Player2SpawnPoint.transform.position;
        }

        if (other.gameObject.CompareTag("Player3"))
        {
            other.transform.position = Player3SpawnPoint.transform.position;
        }

        if (other.gameObject.CompareTag("Player4"))
        {
            other.transform.position = Player4SpawnPoint.transform.position;
        }
    }
    
}
