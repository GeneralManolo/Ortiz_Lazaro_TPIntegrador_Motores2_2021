﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player2GameManager : MonoBehaviour
{
    public float Timer;
    public float StartTimer;

    public Player1_Controller player1;
    public Player2_Controller player2;

    public static float Player1Score;
    public static float Player2Score;

    public Text TimerText;
    public Text ScorePlayer1Text;
    public Text ScorePlayer2Text;

    public Text Player1ScoreCounter;
    public Text Player2ScoreCounter;

    public GameObject GameOver;

    public Text StarterTimer;
    void Start()
    {
        player1 = GameObject.FindObjectOfType<Player1_Controller>();
        player2 = GameObject.FindObjectOfType<Player2_Controller>();

        Timer = 60;
        StartTimer = 3;

        GameOver.gameObject.SetActive(false);
    }
    void Update()
    {
        if (StartTimer <= 0)
        {
            player1.enabled = true;
            player2.enabled = true;

            Timer -= Time.deltaTime;

            StarterTimer.gameObject.SetActive(false);
        }
        else
        {
            StartTimer -= Time.deltaTime;
            player1.enabled = false;
            player2.enabled = false;
        }

        StarterTimer.text = StartTimer.ToString("0");

        TimerText.text = "Time left: " + Timer.ToString("0");

        ScorePlayer1Text.text = "Score: " + Player1Score;
        ScorePlayer2Text.text = "Score: " + Player2Score;

        if (Timer <= 0)
        {
            Time.timeScale = 0;
            GameOver.gameObject.SetActive(true);

            Player1ScoreCounter.text = "Player 1 painted: " + Player1Score + " tiles";
            Player2ScoreCounter.text = "Player 2 painted: " + Player2Score + " tiles";
        }

        if (Player1Score < 0)
        {
            Player1Score = 0;
        }

        if (Player2Score < 0)
        {
            Player2Score = 0;
        }
    }
}
