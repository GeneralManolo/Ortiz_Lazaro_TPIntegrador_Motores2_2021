﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingManager : MonoBehaviour
{
    public Vector3[] points;
    public int pointnumber;
    private Vector3 target;

    public float tolerance;
    public float speed;
    public float delay;
    public float startdelay;

    public bool automatic;

    void Start()
    {
        pointnumber = 0;

        if (points.Length > 0)
        {
            target = points[0];
        }
        tolerance = speed * Time.deltaTime;
    }

    void Update()
    {
        if (transform.position != target)
        {
            MovePlatform();
        }
        else
        {
            UpdateTarget();
        }
    }

    public void MovePlatform()
    {
        Vector3 heading = target - transform.position;
        transform.position += (heading / heading.magnitude * speed * Time.deltaTime);
        if (heading.magnitude < tolerance)
        {
            transform.position = target;
            startdelay = Time.time;
        }
    }

    public void UpdateTarget()
    {
        if (automatic)
        {
            if (Time.time > startdelay)
            {
                NextPlatform();
            }       
        }
    }

    public void NextPlatform()
    {
        pointnumber++;
        if (pointnumber >= points.Length)
        {
            pointnumber = 0;
        }

        target = points[pointnumber];
    }

}
