﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KOTHPlayer3 : MonoBehaviour
{
    public float speed;

    void Update()
    {
        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal3"), 0, Input.GetAxisRaw("Vertical3"));
        transform.position = Vector3.MoveTowards(transform.position, transform.position + movement, speed * Time.deltaTime);
    }
}
