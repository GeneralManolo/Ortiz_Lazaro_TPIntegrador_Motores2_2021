﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KOTHGameManager : MonoBehaviour
{
    public float StartTimer;

    public KOTHPlayer1 player1;
    public KOTHPlayer2 player2;
    public KOTHPlayer3 player3;
    public KOTHPlayer4 player4;

    public static float Player1Score;
    public static float Player2Score;
    public static float Player3Score;
    public static float Player4Score;

    public Text ScorePlayer1Text;
    public Text ScorePlayer2Text;
    public Text ScorePlayer3Text;
    public Text ScorePlayer4Text;

    public GameObject GameOver;

    public Text StarterTimer;

    public Text WinnerText;
    void Start()
    {
        player1 = GameObject.FindObjectOfType<KOTHPlayer1>();
        player2 = GameObject.FindObjectOfType<KOTHPlayer2>();
        player3 = GameObject.FindObjectOfType<KOTHPlayer3>();
        player4 = GameObject.FindObjectOfType<KOTHPlayer4>();

        StartTimer = 3;

        GameOver.gameObject.SetActive(false);
    }
    void Update()
    {
        if (StartTimer <= 0)
        {
            player1.enabled = true;
            player2.enabled = true;
            player3.enabled = true;
            player4.enabled = true;

            StarterTimer.gameObject.SetActive(false);
        }
        else
        {
            StartTimer -= Time.deltaTime;
            player1.enabled = false;
            player2.enabled = false;
            player3.enabled = false;
            player4.enabled = false;
        }

        StarterTimer.text = StartTimer.ToString("0");

        ScorePlayer1Text.text = "Score: " + Player1Score;
        ScorePlayer2Text.text = "Score: " + Player2Score;
        ScorePlayer3Text.text = "Score: " + Player3Score;
        ScorePlayer4Text.text = "Score: " + Player4Score;

        if (Player1Score < 0)
        {
            Player1Score = 0;
        }

        if (Player2Score < 0)
        {
            Player2Score = 0;
        }

        if (Player3Score < 0)
        {
            Player3Score = 0;
        }

        if (Player4Score < 0)
        {
            Player4Score = 0;
        }

        if (Player1Score >= 500)
        {
            Time.timeScale = 0;
            GameOver.gameObject.SetActive(true);
            WinnerText.color = Color.blue;
            WinnerText.text = "Player 1 is the king of the hill!";
        }

        if (Player2Score >= 500)
        {
            Time.timeScale = 0;
            GameOver.gameObject.SetActive(true);
            WinnerText.color = Color.red;
            WinnerText.text = "Player 2 is the king of the hill!";
        }

        if (Player3Score >= 500)
        {
            Time.timeScale = 0;
            GameOver.gameObject.SetActive(true);
            WinnerText.color = Color.yellow;
            WinnerText.text = "Player 3 is the king of the hill!";
        }

        if (Player4Score >= 500)
        {
            Time.timeScale = 0;
            GameOver.gameObject.SetActive(true);
            WinnerText.color = Color.green;
            WinnerText.text = "Player 4 is the king of the hill!";
        }
    }
}
