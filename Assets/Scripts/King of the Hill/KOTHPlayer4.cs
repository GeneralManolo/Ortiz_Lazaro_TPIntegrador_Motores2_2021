﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KOTHPlayer4 : MonoBehaviour
{
    public float speed;

    void Update()
    {
        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal4"), 0, Input.GetAxisRaw("Vertical4"));
        transform.position = Vector3.MoveTowards(transform.position, transform.position + movement, speed * Time.deltaTime);
    }
}
