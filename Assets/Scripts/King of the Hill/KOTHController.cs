﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KOTHController : MonoBehaviour
{
    private Renderer render;

    public Material NoColor;
    public Material Player1Color;
    public Material Player2Color;
    public Material Player3Color;
    public Material Player4Color;

    void Start()
    {
        render = GetComponent<Renderer>();
        render.material = NoColor;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            gameObject.tag = "Blue";
            render.material = Player1Color;

            KOTHGameManager.Player1Score += 1;
        }

        if (other.gameObject.CompareTag("Player2"))
        {
            gameObject.tag = "Red";
            render.material = Player2Color;

            KOTHGameManager.Player2Score += 1;
        }

        if (other.gameObject.CompareTag("Player3"))
        {
            gameObject.tag = "Yellow";
            render.material = Player3Color;

            KOTHGameManager.Player3Score += 1;
        }

        if (other.gameObject.CompareTag("Player4"))
        {
            gameObject.tag = "Green";
            render.material = Player4Color;

            KOTHGameManager.Player4Score += 1;
        }
    }
}
