﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public class Persistence : MonoBehaviour
{
    public static Persistence persistence;
    public PersistenceData data;
    string DataArchive = "save.dat";

    private void Awake()
    {
        if (persistence == null)
        {
            DontDestroyOnLoad(this.gameObject);
            persistence = this;
        }
        else if (persistence != this)
            Destroy(this.gameObject);

        LoadPersistence();
    }

    public void SavePersistence()
    {
        string filePath = Application.persistentDataPath + "/" + DataArchive;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
    }

    public void LoadPersistence()
    {
        string filePath = Application.persistentDataPath + "/" + DataArchive;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            PersistenceData cargado = (PersistenceData)bf.Deserialize(file);
            data = cargado;
            file.Close();
        }
    }
}

[System.Serializable]
public class PersistenceData
{
    public float Score;
    public PersistenceData()
    {
        Score = 0;
    }
}
