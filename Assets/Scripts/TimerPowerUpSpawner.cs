﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerPowerUpSpawner : MonoBehaviour
{
    public GameObject timePowerUp;

    public Vector3 center;
    public Vector3 size;

    public bool respawn;
    public float spawnDelay;
    private float currentTime;
    private bool spawning;
    void Start()
    {
        Spawn();
        currentTime = spawnDelay;
    }

    private void Update()
    {
        if (spawning)
        {
            currentTime -= Time.deltaTime;
        }

        if (currentTime <= 0)
        {
            Spawn();
        }
    }

    public void Respawn()
    {
        spawning = true;
        currentTime = spawnDelay;
    }

    public void Spawn()
    {
        Vector3 spawnTime = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));

        Instantiate(timePowerUp, spawnTime, Quaternion.identity);

        spawning = true;
        currentTime = spawnDelay;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0, 0, 1, 0.5f);
        Gizmos.DrawCube(center, size);
    }

}
