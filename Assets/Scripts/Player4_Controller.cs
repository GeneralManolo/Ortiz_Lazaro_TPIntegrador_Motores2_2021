﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player4_Controller : MonoBehaviour
{
    public float speed;

    private Vector3 scaleChange;

    private bool SpeedUp;
    private bool SizeUp;
    private bool TimeOut;

    public float SpeedTimer;
    public float SizeTimer;
    public float TimeTimer;

    public float MaxSpeed;
    public float MaxTimer;

    public Player1_Controller player1;
    public Player2_Controller player2;
    public Player3_Controller player3;
    
    void Start()
    {
        SpeedUp = false;

        scaleChange = new Vector3(1, 1, 1);

    }
    void Update()
    {
        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal4"), 0, Input.GetAxisRaw("Vertical4"));
        transform.position = Vector3.MoveTowards(transform.position, transform.position + movement, speed * Time.deltaTime);

        if (SpeedUp == true)
        {
            SpeedTimer -= Time.deltaTime;
        }

        if (speed > 40)
        {
            speed = 40;
        }

        if (SizeUp == true)
        {
            SizeTimer -= Time.deltaTime;
        }

        if (TimeOut == true)
        {
            TimeTimer -= Time.deltaTime;
            player1.enabled = false;
            player2.enabled = false;
            player3.enabled = false;
        }

        if (SpeedTimer <= 0)
        {
            SpeedUp = false;
            SpeedTimer = MaxTimer;
            speed = MaxSpeed;
        }

        if (SizeTimer <= 0)
        {
            SizeUp = false;
            SizeTimer = MaxTimer;
            gameObject.transform.localScale = scaleChange;
        }

        if (TimeTimer <= 0)
        {
            TimeOut = false;
            TimeTimer = MaxTimer;

            player1.enabled = true;
            player2.enabled = true;
            player3.enabled = true;
        }

    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Haste"))
        {
            Destroy(other.gameObject);
            SpeedUp = true;

            if (SpeedUp == true)
            {
                speed += speed;
            }
        }

        if (other.gameObject.CompareTag("Size"))
        {
            Destroy(other.gameObject);
            SizeUp = true;

            if (SizeUp == true)
            {
                gameObject.transform.localScale += scaleChange;
            }

        }

        if (other.gameObject.CompareTag("Time"))
        {
            Destroy(other.gameObject);
            TimeOut = true;

            player1 = GameObject.FindObjectOfType<Player1_Controller>();
            player2 = GameObject.FindObjectOfType<Player2_Controller>();
            player3 = GameObject.FindObjectOfType<Player3_Controller>();
        }



        if (other.gameObject.CompareTag("Blue"))
        {
            GameManager.Player1Score -= 1;
        }

        if (other.gameObject.CompareTag("Red"))
        {
            GameManager.Player2Score -= 1;
        }

        if (other.gameObject.CompareTag("Yellow"))
        {
            GameManager.Player3Score -= 1;
        }
    }
}
