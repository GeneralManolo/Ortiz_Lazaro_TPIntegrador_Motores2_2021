﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public float Timer;
    public float StartTimer;
    public Player1_Controller player1;
    public Player2_Controller player2;
    public Player3_Controller player3;
    public Player4_Controller player4;

    public static float Player1Score;
    public static float Player2Score;
    public static float Player3Score;
    public static float Player4Score;

    public Text TimerText;
    public Text ScorePlayer1Text;
    public Text ScorePlayer2Text;
    public Text ScorePlayer3Text;
    public Text ScorePlayer4Text;

    public Text Player1ScoreCounter;
    public Text Player2ScoreCounter;
    public Text Player3ScoreCounter;
    public Text Player4ScoreCounter;

    public GameObject GameOver;

    public Text StarterTimer;
    void Start()
    {
        player1 = GameObject.FindObjectOfType<Player1_Controller>();
        player2 = GameObject.FindObjectOfType<Player2_Controller>();
        player3 = GameObject.FindObjectOfType<Player3_Controller>();
        player4 = GameObject.FindObjectOfType<Player4_Controller>();

        Timer = 60;
        StartTimer = 3;

        GameOver.gameObject.SetActive(false);
    }
    void Update()
    {
        if (StartTimer <= 0)
        {
            player1.enabled = true;
            player2.enabled = true;
            player3.enabled = true;
            player4.enabled = true;

            Timer -= Time.deltaTime;

            StarterTimer.gameObject.SetActive(false);
        }
        else
        { 
            StartTimer -= Time.deltaTime;
            player1.enabled = false;
            player2.enabled = false;
            player3.enabled = false;
            player4.enabled = false;
        }

        StarterTimer.text = StartTimer.ToString("0");

        TimerText.text = "Time left: " + Timer.ToString("0");

        ScorePlayer1Text.text = "Score: " + Player1Score;
        ScorePlayer2Text.text = "Score: " + Player2Score;
        ScorePlayer3Text.text = "Score: " + Player3Score;
        ScorePlayer4Text.text = "Score: " + Player4Score;

        if (Timer <= 0)
        {
            Time.timeScale = 0;
            GameOver.gameObject.SetActive(true);

            Player1ScoreCounter.text = "Player 1 painted: " + Player1Score + " tiles";
            Player2ScoreCounter.text = "Player 2 painted: " + Player2Score + " tiles";
            Player3ScoreCounter.text = "Player 3 painted: " + Player3Score + " tiles";
            Player4ScoreCounter.text = "Player 4 painted: " + Player4Score + " tiles";

        }

        if (Player1Score < 0)
        {
            Player1Score = 0;
        }

        if (Player2Score < 0)
        {
            Player2Score = 0;
        }

        if (Player3Score < 0)
        {
            Player3Score = 0;
        }

        if (Player4Score < 0)
        {
            Player4Score = 0;
        }
    }
}
